#!/bin/bash

if ! [ -x "$(command -v s3cmd)" ]; then
  	if ! [ -x "$(command -v brew)" ]; then
		# install homebrew
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	fi
	# install s3cmd
	brew install s3cmd
fi

if [[ -z "$S3_BUCKET" ]]; then
   echo 'Empty S3 bucket. Please provide using S3_BUCKET'
   exit 1
fi

if [[ -z "$AWS_ACCESS_KEY_ID" ]]; then
   echo 'Empty AWS access key. Please provide using AWS_ACCESS_KEY_ID'
   exit 1
fi

if [[ -z "$AWS_SECRET_ACCESS_KEY" ]]; then
   echo 'Empty AWS secret key. Please provide using AWS_SECRET_ACCESS_KEY'
   exit 1
fi

FILES="./tmp/*.mp3"

for f in $FILES; do
  folder=$(basename $f ".mp3")
  s3cmd put --acl-public --guess-mime-type $f "$S3_BUCKET/$folder/$folder.mp3"
done
