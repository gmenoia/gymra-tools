#!/bin/bash

if ! [ -x "$(command -v s3cmd)" ]; then
  	if ! [ -x "$(command -v brew)" ]; then
		# install homebrew
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	fi
	# install s3cmd
	brew install s3cmd
fi

if [[ -z "$S3_BUCKET" ]]; then
   echo 'Empty S3 bucket. Please provide using S3_BUCKET'
   exit 1
fi

if [[ -z "$AWS_ACCESS_KEY_ID" ]]; then
   echo 'Empty AWS access key. Please provide using AWS_ACCESS_KEY_ID'
   exit 1
fi

if [[ -z "$AWS_SECRET_ACCESS_KEY" ]]; then
   echo 'Empty AWS secret key. Please provide using AWS_SECRET_ACCESS_KEY'
   exit 1
fi

FILES_DIR="./tmp"

# try to create files dir in case doesn't exist
mkdir -p "$FILES_DIR";

for d in $(s3cmd ls $S3_BUCKET); do
	if [[ $d == *"s3://"* ]]; then
		echo $d
		
		HAS_MP3=0
		MP4=""
		
		for f in $(s3cmd ls $d); do
			if [[ $f == *"s3://"* ]] && [[ $f == *"mp3"* ]]; then
				HAS_MP3=1
			fi
			if [[ $f == *"s3://"* ]] && [[ $f == *"mp4"* ]]; then 
				MP4=$f
			fi
		done
		
		if [ $HAS_MP3 == 0 ] && [[ $MP4 == *"mp4"* ]]; then
			LOCAL_MP4="$files/$(basename $MP4)"
			s3cmd get $MP4 "$LOCAL_MP4" --continue
		fi
	fi
done
