#!/bin/bash

if ! [ -x "$(command -v ffmpeg)" ]; then
  	if ! [ -x "$(command -v brew)" ]; then
		# install homebrew
		/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	fi
	# install ffmpeg
	brew install ffmpeg
fi

FILES="./tmp/*.mp4"

for f in $FILES; do
    MP3="${f/mp4/mp3}"
    echo "Converting $f file into $MP3"
    ffmpeg -i "$f" -b:a 48K -vn "$MP3"
done
