# Gymra Tools

## Requirement
* Mac OS
* AWS access and secret key

## Scripts
Before running the scripts, first make sure to grab the AWS access key id and AWS secret access key from AWS. You can find how to get this on https://supsystic.com/documentation/id-secret-access-key-amazon-s3/ or any other page found on Google.

### 1. Download
Copy the command bellow and past in a text editor and replace the "<put here ...>" tags by the AWS credentials. Finally, copy the command again and paste in the Terminal app in your Mac, then click Enter.
```
AWS_ACCESS_KEY_ID=<put here your AWS access key> AWS_SECRET_ACCESS_KEY=<put here your AWS secret> S3_BUCKET=s3://gymravideos sh download.sh
```
This command should take a while to complete, depending on how many videos to process.

### 2. Convert
After executing the Download script, copy this command and paste in the Terminal app in your Mac, then click Enter.
```
sh convert.sh
```
This command should take a while to complete, depending on how many videos to process.

### 3. Upload
Copy the command bellow and past in a text editor and replace the "<put here ...>" tags by the AWS credentials. Finally, copy the command again and paste in the Terminal app in your Mac, then click Enter.
```
AWS_ACCESS_KEY_ID=<put here your AWS access key> AWS_SECRET_ACCESS_KEY=<put here your AWS secret> S3_BUCKET=s3://gymravideos sh upload.sh
```
This command should take a while to complete, depending on how many videos to process.
